# Descomplic GITLAB PROD

Treinamento descomplicando GITLAB criado ao vivo na Twitch

### Day-1
```bash

  - Entendemos o que e GIT
  - Entendemos o que e Working Dir, Index e HEAD
  - Entendemos o que e o GITLAB
  - Como criar um Grupo no GITLAB
  - Como criar um repositorio GIT
  - Aprendemos os comandos basicos para manipulaçao de arquivos e diretorios no GIT
  - Como criar uma Branch
  - Como criar um Merge Request
  - Como adicionar um Membro no projeto
  - Como fazer o merge na Master/Main
  - Como associar um repo local a um repo remoto
  - Como importar um repo do GITHUB para o GITLAB
  - Mudamos a branch padrao para prod
```  
